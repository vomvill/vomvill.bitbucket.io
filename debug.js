var requestURL = './schedule.json'
var startTime;
var place;
var countDownDate;
var scheDate;
var schedules;
var detail;
$.get(requestURL, function(data) {
    schedules = data;
    var nowESTDate = parseInt(moment.tz("America/New_York").format('DD'), 10);
    var nowESTTime = (new Date(moment.tz("America/New_York").format('YYYY-MM-DDTHH:mm:ssZ'))).getTime();
    var for_sche;
    for (var i = 0; i < schedules.length; i++) {
        var scheDate = new Date(moment.tz(schedules[i]['date'], "America/New_York").format('YYYY-MM-DDTHH:mm:ssZ'));
        var scheDay = parseInt(moment.tz(schedules[i]['date'], "America/New_York").format('DD'), 10);
        if (scheDay == nowESTDate) {
            for_sche = scheDate;
            startTime = moment.tz(schedules[i]['date'], "America/New_York").format('YYYY-MM-DD HH:mm');
            place = schedules[i]['place'];
            detail = schedules[i]['detail'];
            break;
        } else if (scheDate.getTime() > nowESTTime) {
            // cuz. Days is cross the month.
            for_sche = scheDate;
            startTime = moment.tz(schedules[i]['date'], "America/New_York").format('YYYY-MM-DD HH:mm');
            place = schedules[i]['place'];
            detail = schedules[i]['detail'];
            break;
        } else {
            startTime = "Must maintenance..."
        }
    }

    // Set the date we're counting down to
    countDownDate = for_sche.getTime();
    var newYork = moment.tz(startTime, "America/New_York");
    var there = newYork.clone().tz(moment.tz.guess());
    var localTime = there.format("MM/DD hh:mm:ss a z");
    // Set the date starting time.
    document.getElementById("start_time_EST").innerHTML = startTime;
    document.getElementById("start_time").innerHTML = localTime;
    document.getElementById("live_place").innerHTML = "<img src=\"./" + place + ".png\">";
    document.getElementById("detail").innerHTML = "(" + detail + ")";
});

// Update the count down every 1 second
var x = setInterval(function() {
    // Get today's date and time
    var usaTime = (new Date(moment.tz("America/New_York").format('YYYY-MM-DDTHH:mm:ssZ'))).getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - usaTime;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "STARTED!!!";
    }
}, 1000);